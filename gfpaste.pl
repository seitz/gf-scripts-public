#!/usr/bin/perl

#######################################
#                                     #
# Sticky Notes 1.0+ CLI script        #
#                                     #
# Bryan Seitz <seitz@ghettoforge.org> #
#                                     #
# TODO: Add more error handling       #
#                                     #
#######################################
use warnings;
use strict;

use Data::Dumper;
use JSON;
use URI::Escape;
use HTTP::Request::Common;
use HTTP::Response;
use LWP::UserAgent;
use Getopt::Long qw( GetOptions );

my $username     = $ENV{LOGNAME} || $ENV{USER} || getpwuid($<);
my $rest_site    = 'http://p.bsd-unix.net';
my $create_path  = '/api/json/create';
my $opt_title    = "Untitled Paste from $username";
my $opt_data     = '';
my $opt_lang     = 'text';
my $opt_private  = 0;
my $MIN_INPUTLEN = 5;
my %OPTS         = ();

sub usage {
  printf( "Usage: $0 [--title paste title] [--private] [--help]\n   " );
  printf( "\t\t--title:   Title of the paste \*\n                   " );
  printf( "\t\t--lang:    Lanaguage of paste, defaults to Text\n    " );
  printf( "\t\t--private: Whether the paste will be private or not\n" );
  printf( "\t\t--help:    This help message\n\n                     " );
  printf( "\* Text to be pasted will be read in on STDIN\n          " );

  exit( 0 );
}

my $optres = GetOptions( \%OPTS, 'title:s', 'lang:s', 'private', 'help' );

if ( %OPTS ) {
    if ( $optres != 1 || $OPTS{'help'} ) {
        usage();
    }

    if ( $OPTS{'title'} && $OPTS{'title'} =~ m/^(.+)$/ ) {
        $opt_title = $1;
    }

    if ( $OPTS{'lang'} && $OPTS{'lang'} =~ m/^(.+)$/ ) {
        $opt_lang = $1;
    }

    if ( $OPTS{'private'} ) {
        $opt_private = 1;
    }
}

while( <> ) {
    $opt_data .= $_;
}

if ( length( $opt_data ) < $MIN_INPUTLEN ) {
    printf( "Input data too short.\n" );

    usage();
}

my $json_hash = {
    data     => $opt_data,
    language => $opt_lang,
    title    => $opt_title,
    private  => $opt_private,
    expire   => 0,
};

my $json     = JSON->new->utf8->allow_nonref;
my $jsonData = $json->encode( $json_hash );
my $req      = HTTP::Request->new( 'POST', $rest_site . $create_path );

$req->header( 'Content-Type' => 'application/json' );
$req->content( $jsonData);

my $lwp      = LWP::UserAgent->new;
my $response = $lwp->request( $req );

if ( $response->is_success ) {
    my $json_response = $json->decode( $response->content() );
    my $res = $json_response->{'result'};
    if ( ! exists( $res->{'error'} ) ) {
        printf( "%s/%s\n", $rest_site, $opt_private ? $res->{'id'} . '/' . $res->{'hash'} : $res->{'id'} );
    } else {
        printf( "Error: %s\n", $res->{'error'} );
    }
} else {
    print "Failed to paste, fatal error.\n";
    print Dumper( $response );
}

exit( 0 );
### EOF
