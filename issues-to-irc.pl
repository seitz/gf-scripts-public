#!/usr/bin/perl

### CGI to receive POST events from the GhettoForge JIRA webhook system

use CGI ":all";
use Data::Dumper;
use IO::Socket::INET;
use JSON::Parse 'parse_json';
use Log::Log4perl;
use warnings;

my $irc_host = 'BOT.IRC.IP';
my $irc_port = BOT_IRC_PORT;
my $irc_chan = '#CHANNEL_TO_SPAM';
$|           = 1;

my $WEBHOOK_EVENTS = {
    'jira:issue_created' => 'Issue created',
    'jira:issue_deleted' => 'Issue deleted',
    'jira:issue_updated' => 'Issue updated',
    'jira:worklog_updated' => 'Worklog updated',
};

### Initialize Logger
Log::Log4perl->init(\ qq{
    log4perl.category                           = INFO, Logfile
    log4perl.appender.Logfile                   = Log::Dispatch::FileRotate
    log4perl.appender.Logfile.mode              = append
    log4perl.appender.Logfile.filename          = /var/log/issues-to-irc/access.log
    log4perl.appender.Logfile.max               = 14
    log4perl.appender.Logfile.DatePattern       = yyyy-MM-dd
    log4perl.appender.Logfile.TZ                = GMT
    log4perl.appender.Logfile.layout            = Log::Log4perl::Layout::PatternLayout
    log4perl.appender.Logfile.layout.ConversionPattern = %d (%r) [%p] %m %n
});
my $logger = Log::Log4perl->get_logger();

$logger->info( "$0 request received" );

if ( ! defined( $ENV{'REQUEST_METHOD'} ) ) {
    $logger->error( "Script not invoked from web, exiting" );
    exit( 1 );
}

sub log_to_irc( $ ) {
    my $msg  = shift || return( -1 );
    my $sock = IO::Socket::INET->new(  
                                        PeerPort => $irc_port,
                                        PeerAddr => $irc_host,
                                        Proto    => 'tcp',
                                        Timeout  => 5,
    );
    if ( !$sock ) {
        $logger->info( "Could not create socket object for IRC logging" );
        return( -1 );
    } else {
        my $buf = sprintf( "%s %s %s", $irc_chan, '[JIRA]', $msg );
        print $sock $buf . "\n";
        $logger->info( "To IRC: $buf" );
        $sock->close();
    }
}


my $q = CGI->new;

### Deny non POST requests
### TODO: Add IP / user agent checks to limit access
if ( $ENV{'REQUEST_METHOD'} ne 'POST' ) {
    $logger->info( "Non POST request method used, exiting" );
    print $q->header( -status => '404 Forbidden' );
    exit( 1 );
}

print $q->header();

my $atlassian_json = $q->param('POSTDATA');
my $perl_data      = parse_json( $atlassian_json );
my $change_user    = $q->url_param( 'user_id' );

chomp( $perl_data->{'comment'}->{'body'} );

my $logbuf = sprintf( "Issue %s has been modified with action [%s] by %s [%s]: %s", 
                      $perl_data->{'issue'}->{'key'},
                      $WEBHOOK_EVENTS->{$perl_data->{'webhookEvent'}},
                      $perl_data->{'user'}->{displayName},
                      $perl_data->{'issue'}->{'fields'}->{'status'}->{'name'},
                      $perl_data->{'comment'}->{'body'} );

log_to_irc( $logbuf );

$logbuf = sprintf( "%s (%s)", 
                   'https://ghettoforge.atlassian.net/browse/' . $perl_data->{'issue'}->{'key'},
                   $perl_data->{'issue'}->{'fields'}->{'summary'} );

log_to_irc( $logbuf );

$logger->info( "$0 done" );

exit( 0 );
### EOF
